use std::fs;
use std::str;
use std::error::Error;
use clap::{Arg, App};
use parser::Parser;

mod parser;
mod pretty_print;
mod translate;
mod dictionaries;
mod wrapper;

#[macro_use]
extern crate lazy_static;

fn main() -> Result<(), Box<dyn Error>>{
    let matches = App::new("Stardew Analyzer")
        .version("1.0")
        .author("Ben Allen <contact@thebenallen.net>")
        .about("Dumps important information from Stardew Valley save files")
        .arg(Arg::with_name("inventory")
            .short("i")
            .long("inventory")
            .help("Displays inventory contents"))
        .arg(Arg::with_name("stats")
            .short("s")
            .long("stats")
            .help("Displays game statistics"))
        .arg(Arg::with_name("level")
            .short("l")
            .long("level")
            .help("Displays player level information"))
        .arg(Arg::with_name("fish")
            .short("f")
            .long("fish")
            .help("Displays whether fish have been caught or not"))
        .arg(Arg::with_name("quest")
            .short("q")
            .long("quest")
            .help("Displays the quest journal"))
        .arg(Arg::with_name("FILE")
            .required(true)
            .help("Stardew Valley save file"))
        .get_matches();

    let file_name = matches.value_of("FILE").unwrap();
    let data = read_file(file_name);

    let mut parsed = Parser::new(data);

    if matches.is_present("inventory") {
        parsed.root.player.inventory = parsed.get_inventory()?;
        println!("{}", parsed.root.player.inventory);
    }
    if matches.is_present("stats") {
        parsed.root.player.stats = parsed.get_stats()?;
        println!("{}", parsed.root.player.stats);
    }
    if matches.is_present("level") {
        parsed.root.player.level = parsed.get_level_info();
        println!("{}", parsed.root.player.level);
    }
    if matches.is_present("fish") {
        parsed.root.player.fish_caught = parsed.get_fish_caught()?;
        println!("{}", parsed.root.player.fish_caught);
    }
    if matches.is_present("quest") {
        parsed.root.player.quest_log = parsed.get_current_quests()?;
        println!("{}", parsed.root.player.quest_log);
    }

    Ok(())
}

fn read_file(name: &str) -> String {
    let raw_data = fs::read_to_string(name).expect("Failed to read the file");
    let document_start = raw_data.find('<').expect("Could not find start of the document");
    raw_data[document_start..].to_owned()
}
