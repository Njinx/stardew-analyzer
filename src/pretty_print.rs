use std::fmt::{self, Display};
use crate::parser::{self, Wrapper};

const INDENT_SIZE: usize = 4;

fn generate_indent(level: usize) -> String {
    " ".repeat(INDENT_SIZE*level)
}

impl Display for parser::Stats {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let header_indent = generate_indent(self.indent);
        writeln!(f, "{}{}", header_indent, self.name)?;

        let entry_indent = generate_indent(self.indent+1);
        for entry in &self.unordered {
            writeln!(f, "{}{}: {}", entry_indent, entry.0, entry.1)?;
        }

        if !self.nested.is_empty() {
            for nested_stats in &self.nested {
                nested_stats.fmt(f)?
            }  
        }

        Ok(())
    }
}

impl Display for parser::Inventory {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "Inventory")?;

        let entry_indent = generate_indent(1);
        for item in self.unpack() {
            writeln!(f, "{}{} ({})", entry_indent, item.name, item.count)?;
        }

        Ok(())
    }
}

impl Display for parser::Level {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "Player Level")?;

        let entry_indent = generate_indent(1);
        writeln!(f, "{}Farming: {}", entry_indent, self.farming)?;
        writeln!(f, "{}Mining: {}", entry_indent, self.mining)?;
        writeln!(f, "{}Combat: {}", entry_indent, self.combat)?;
        writeln!(f, "{}Foraging: {}", entry_indent, self.foraging)?;
        writeln!(f, "{}Fishing: {}", entry_indent, self.fishing)?;

        Ok(())
    }
}

impl Display for parser::FishCaught {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "Fish Caught")?;

        let entry_indent = generate_indent(1);
        for item in self.unpack() {
            let pretty_bool = |&b| if b { "Yes" } else { "No" };
            writeln!(f, "{}{}: {}", entry_indent, item.0, pretty_bool(item.1))?;
        }

        Ok(())
    }
}

impl Display for parser::QuestReward {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Money(money) => { write!(f, "{}", money)?; },
            Self::Item(item) => { write!(f, "{}", item)?; },
            Self::None => { write!(f, "None")?; },
        }

        Ok(())
    }
}

impl Display for parser::Quest {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let days_left_fmt = |x: u8| if x == 0 { "Unlimited".to_owned() } else { x.to_string() };

        writeln!(f, "{}", self.title)?;
        writeln!(f, "{}\n", self.description)?;
        writeln!(f, "{}", self.objective)?;
        writeln!(f, "Days Left: {}", days_left_fmt(self.days_left))?;
        match &self.reward {
            parser::QuestReward::Money(val) => { writeln!(f, "Reward: {}g", val)?; },
            parser::QuestReward::Item(val) => { writeln!(f, "Reward: {}", val)?; },
            parser::QuestReward::None => {},
        }

        Ok(())
    }
}

impl Display for parser::QuestLog {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "Quests\n")?;

        for quest in self.unpack() {
            writeln!(f, "{}", quest)?;
        }

        Ok(())
    }
}