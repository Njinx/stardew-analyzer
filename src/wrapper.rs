#![macro_use]

// wrap!(WRAPPER_NAME(UNDERLYING_TYPE));
// wrap!(WRAPPER_NAME(UNDERLYING_TYPE) [DERIVES]);
#[macro_export]
macro_rules! wrap {
    ( $outer:ident($inner:ty) $( [$($derive:meta),+] )? ) => {
        $( #[derive( $( $derive ),+ )] )?
        pub struct $outer(pub $inner);

        impl Wrapper for $outer {
            type Inner = $inner;

            fn unpack(&self) -> &Self::Inner {
                &self.0
            }
        }
    };
}
