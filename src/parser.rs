use std::collections::HashMap;
use xmltree::{Element, XMLNode};
use std::cmp::Ordering;

pub struct Parser {
    dom: Element,
    pub root: Root,
}

impl Parser {
    pub fn new(data: String) -> Self {
        Parser {
            dom: Self::parse_xml(data),
            root: Default::default(),
        }
    }

    fn parse_xml(data: String) -> Element {
        Element::parse(data.as_bytes()).expect("Failed to parse XML")
    }

    fn get_raw_player_data(&self) -> Result<&Element, String> {
        match self.dom.get_child("player") {
            Some(val) => Ok(val),
            None => Err("Failed to retrieve player data".to_owned()),
        }
    }

    pub fn get_player_info(&self, key: &str) -> Result<String, String> {
        let player_data = self.get_raw_player_data()?;
        match player_data.get_child(key) {
            Some(val) => match val.get_text() {
                Some(val) => Ok(val.to_string()),
                None => Err(format!("Key is not assigned a value: {:?}", val)),
            },
            None => Err(format!("Key does not exist: {}", key)),
        }
    }
    
    pub fn get_level_info(&self) -> Level {
        let result_helper = |key| self.get_player_info(key)
            .unwrap_or_else(|err| format!("Failed to get {} level: {}", key, err));
    
        Level {
            farming: result_helper("farmingLevel"),
            mining: result_helper("miningLevel"),
            combat: result_helper("combatLevel"),
            foraging: result_helper("foragingLevel"),
            fishing: result_helper("fishingLevel"),
        }
    }
    
    pub fn get_inventory(&self) -> Result<Inventory, String> {
        let player_data = self.get_raw_player_data()?;
        let raw_items = match player_data.get_child("items") {
            Some(val) => val.children.clone(),
            None => Vec::<XMLNode>::new(),
        };
        let mut inventory = Vec::<Item>::new();
        for item in raw_items {
            let item_attr = |key: &str| item.as_element().unwrap().get_child(key)
                .map(|val| val.get_text().unwrap().to_string());
    
            let item_name = item_attr("DisplayName");
            let item_count = item_attr("stack");
    
            if let Some(val) = item_name {
                let parsed_item = Item {
                    name: val,
                    count: item_count.unwrap_or_else(|| "1".to_owned()).parse::<u16>().unwrap(),
                };
                inventory.push(parsed_item);
            }
        }
    
        Ok(Inventory(inventory))
    }
    
    pub fn get_stats(&self) -> Result<Stats, String> {
        let player_data = self.get_raw_player_data()?;
        let raw_stats = match player_data.get_child("stats") {
            Some(val) => val.children.clone(),
            None => Vec::<XMLNode>::new(),
        };
        let mut stats: Stats = Default::default();
        for stat in raw_stats {
            let stat_elem = stat.as_element().unwrap();
            let key = crate::translate::from_codename(stat_elem.name.clone());
            let val: String;
    
            if key == "stat_dictionary" {
                let partial_map = Self::unpack_dict(stat_elem);
                stats.unordered.extend(partial_map);
            } else if stat_elem.children.iter().filter(|x| x.as_element().is_some()).count() > 0 {
                let nested_stats = Stats {
                    name: key.clone(),
                    unordered: Self::unpack_dict(stat_elem),
                    indent: 1,
                    ..Default::default()
                };
                stats.nested.push(nested_stats);
            } else {
                val = match stat_elem.get_text() {
                    Some(v) => v.to_string(),
                    None => "0".to_owned(),
                };
                stats.unordered.insert(key, val);
            }
        }
    
        Ok(stats)
    }
    
    pub fn get_fish_caught(&self) -> Result<FishCaught, String> {
        let player_data = self.get_raw_player_data()?;
        let raw_fish = match player_data.get_child("fishCaught") {
            Some(val) => val.children.clone(),
            None => Vec::<XMLNode>::new(),
        };

        lazy_static! {
            static ref KNOWN_FISH_DICT: HashMap<i16, &'static str> = {
                let mut tmp: HashMap<i16, &str> = HashMap::new();
                crate::import_fish_id_table!(tmp);
                tmp
            };
        }

        let mut fish_caught = HashMap::<String, bool>::new();
        for fish in raw_fish {
            let fish_id = fish
                .as_element().unwrap()
                .get_child("key").unwrap()
                .children
                .get(0).unwrap()
                .as_element().unwrap()
                .get_text().unwrap()
                .parse::<i16>().unwrap();
            
                if KNOWN_FISH_DICT.get(&fish_id).is_some() {
                    fish_caught.insert(crate::translate::from_item_id(fish_id), true);
                } else {
                    fish_caught.insert(crate::translate::from_item_id(fish_id), false);
                }
        }

        Ok(FishCaught(fish_caught))
    }

    pub fn get_current_quests(&self) -> Result<QuestLog, String> {
        #![allow(clippy::field_reassign_with_default)]
        let player_data = self.get_raw_player_data()?;
        let raw_quest_log = match player_data.get_child("questLog") {
            Some(val) => val.children.clone(),
            None => Vec::<XMLNode>::new(),
        };

        let mut quest_log: Vec<Quest> = Vec::new();
        for quest in raw_quest_log {
            let quest_elem = quest.as_element().unwrap();
            let mut parsed_quest: Quest = Default::default();

            macro_rules! tag_fallback_helper {
                () => { "".to_owned() };
                ( $head:literal $($tail:literal)* ) => {
                    match quest_elem.get_child($head) {
                        Some(val) => match val.get_text() {
                            Some(val) => val.to_string(),
                            None => tag_fallback_helper!($($tail)*),
                        },
                        None => "".to_owned(),
                    }
                };
            }

            parsed_quest.title = tag_fallback_helper!("_questTitle" "questTitle");
            parsed_quest.objective = tag_fallback_helper!("_currentObjective");
            parsed_quest.description = tag_fallback_helper!("_questDescription");
            parsed_quest.reward = match quest_elem.get_child("moneyReward") {   // wtf
                Some(val) => match val.get_text() {
                    Some(val) => match val.parse::<u32>().unwrap().cmp(&0) {
                        Ordering::Greater => QuestReward::Money(val.parse::<u32>().unwrap()),
                        _ => QuestReward::None,
                    },
                    None => match quest_elem.get_child("reward") {
                        Some(val) => match val.get_text() {
                            Some(val) => match val.parse::<u32>().unwrap().cmp(&0) {
                                Ordering::Greater => QuestReward::Money(val.parse::<u32>().unwrap()),
                                _ => QuestReward::None,
                            },
                            None => QuestReward::None,
                        },
                        None => QuestReward::None,
                    },
                },
                None => QuestReward::None,
            };
            parsed_quest.days_left = match quest_elem.get_child("daysLeft") {
                Some(val) => match val.get_text() {
                    Some(val) => val.parse::<u8>().unwrap(),
                    None => 0,
                },
                None => 0,
            };
            quest_log.push(parsed_quest);
        }

        Ok(QuestLog(quest_log))
    }
    
    // TODO: Holy shit for the love of god refactor this disgrace
    fn unpack_dict(elem: &Element) -> HashMap<String, String> {
        let mut dict = HashMap::<String, String>::new();
        for item in &elem.children {
            let item_elem = item.as_element().unwrap();
    
            let raw_key = item_elem.children.get(0).unwrap().as_element().unwrap();
            let raw_val = item_elem.children.get(1).unwrap().as_element().unwrap();
    
            let key = raw_key.children.get(0).unwrap().as_element().unwrap().get_text().unwrap();
            let val = raw_val.children.get(0).unwrap().as_element().unwrap().get_text().unwrap();
            dict.insert(crate::translate::from_codename(key.to_string()), val.to_string());
        }
    
        dict
    }
}

pub trait Wrapper {
    type Inner;

    fn unpack(&self) -> &Self::Inner;
}

#[derive(Debug, Default)]
pub struct Root {
    pub player: Player,
}

#[derive(Debug, Default)]
pub struct Player {
    pub name: String,
    pub farm_name: String,
    pub favorite_thing: String,
    pub inventory: Inventory,
    pub stats: Stats,
    pub level: Level,
    pub fish_caught: FishCaught,
    pub quest_log: QuestLog,
}

crate::wrap!(Inventory(Vec<Item>) [Debug, Default]);
crate::wrap!(FishCaught(HashMap<String, bool>) [Debug, Default]);
crate::wrap!(QuestLog(Vec<Quest>) [Debug, Default]);

#[derive(Debug, Default)]
pub struct Item {
    pub name: String,
    pub count: u16,
}

#[derive(Debug, Default)]
pub struct Level {
    pub farming: String,
    pub mining: String,
    pub combat: String,
    pub foraging: String,
    pub fishing: String,
}

#[derive(Debug)]
pub struct Stats {
    pub name: String,
    pub unordered: HashMap<String, String>,
    pub nested: Vec<Stats>,
    pub indent: usize,
}

impl Default for Stats {
    fn default() -> Self {
        Stats {
            name: String::from("Unorganized Stats"),
            unordered: HashMap::<String, String>::new(),
            nested: Vec::<Stats>::new(),
            indent: 0,
        }
    }
}

#[derive(Debug, Default)]
pub struct Quest {
    pub title: String,
    pub objective: String,
    pub description: String,
    pub reward: QuestReward,
    pub days_left: u8,
}

#[derive(Debug)]
pub enum QuestReward {
    Money(u32),
    Item(String),
    None,
}

impl Default for QuestReward {
    fn default() -> Self {
        QuestReward::None
    }
}
