use std::collections::HashMap;

pub fn from_codename(code_name: String) -> String {
    lazy_static! {
        static ref DICT: HashMap<&'static str, &'static str> = {
            let mut tmp: HashMap<&'static str, &'static str> = HashMap::new();
            crate::import_codename_table!(tmp);
            tmp
        };
    }

    match DICT.get(code_name.as_str()) {
        Some(val) => (*val).to_owned(),
        None => code_name,
    }
}

pub fn from_item_id(id: i16) -> String {
    lazy_static! {
        static ref DICT: HashMap<i16, &'static str> = {
            let mut tmp: HashMap<i16, &'static str> = HashMap::new();
            crate::import_id_table!(tmp);
            tmp
        };
    }

    match DICT.get(&id) {
        Some(val) => (*val).to_owned(),
        None => format!("[{}]", &id),
    }
}